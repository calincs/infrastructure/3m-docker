# X3ML 3M chart

This project is based on [https://github.com/ymark/3M-docker](https://github.com/ymark/3M-docker)

The Docker image `marketak/3m-docker` is located here: [https://hub.docker.com/r/marketak/3m-docker](https://hub.docker.com/r/marketak/3m-docker)

A `docker-compose.yml` file, a Kubernetes Helm chart, and more control over deployment parameters were added.

## Run locally

You will need git and docker installed.

```bash
git clone https://gitlab.com/calincs/infrastructure/3m-docker.git
cd 3m-docker
docker compose up
```

Then go to http://localhost/3M and create a user. 3M and exist data will be stored in `<project folder>/data`.
